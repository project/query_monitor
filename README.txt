
Query Monitor
=============

A query monitor/logger module that utilizes devel module to save the SQL queries used for configuring Drupal. These queries could then be repeated downstream, either directly or in update hooks.
